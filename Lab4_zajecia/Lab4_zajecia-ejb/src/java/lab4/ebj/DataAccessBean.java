
package lab4.ebj;
import java.util.Date;
import javax.ejb.Stateless;

@Stateless
public class DataAccessBean implements DataAccessBeanRemote {
    
    private String OstatnieDane;
    private Date OstatniDostep;
    
    @Override
    public String getOstatnieDane()
    {
        return OstatniDostep + "\n" + OstatnieDane;
    }
    
    @Override
    public void setOstatnieDane(String dane, Date data) 
    {
        OstatnieDane = dane;
        OstatniDostep = data;
    }
}
