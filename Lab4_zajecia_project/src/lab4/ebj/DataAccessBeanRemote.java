/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/SessionRemote.java to edit this template
 */
package lab4.ebj;
import java.util.Date;
import javax.ejb.Remote;

/**
 *
 * @author maciej
 */
@Remote
public interface DataAccessBeanRemote {
    
    public String getOstatnieDane();
    public void setOstatnieDane(String dane, Date data) ;
    
}
